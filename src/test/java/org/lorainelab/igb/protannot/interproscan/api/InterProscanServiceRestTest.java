/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.protannot.interproscan.api;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.lorainelab.igb.protannot.interproscan.InterProscanServiceRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;


/**
 *
 * @author Tarun
 */
public class InterProscanServiceRestTest {

    private static final Logger logger = LoggerFactory.getLogger(InterProscanServiceRestTest.class);

    @Disabled
    @Test
    public void testStatus() {
        InterProscanService service = new InterProscanServiceRest();
        InterProscanService.Status status = service.status("iprscan5-R20150629-154703-0868-18931209-oy");
        assertEquals(status, InterProscanService.Status.FINISHED);
    }


//    @Test
//    public void testRun() {
//        InterProscanService service = new InterProscanServiceRest();
//        JobRequest request = new JobRequest();
//        request.setEmail("tmall@uncc.edu");
//        ParameterType applParameters = service.getApplications();
//        List<ValueType> applValues = applParameters.getValues().getValue();
//        Set<String> inputApplSet = new HashSet<>();
//        for(ValueType valueType : applValues) {
//            inputApplSet.add(valueType.getValue());
//        }
//        
//        request.setSignatureMethods(Optional.of(inputApplSet));
//        request.setTitle(Optional.empty());
//        request.setGoterms(Optional.empty());
//        request.setPathways(Optional.empty());
//        request.setSequence(Optional.of("MSKLPRELTRDLERSLPAVASLGSSLSHSQSLSSHLLPPPEKRRAISDVRRTFCLFVTFDLLFISLLWIIELNTNTGIRKNLEQEIIQYNFKTSFFDIFVLAFFRFSGLLLGYAVLRLRHWWVIALLSKGAFGYLLPIVSFVLAWLETWFLDFKVLPQEAEEERWYLAAQVAVARGPLLFSGALSEGQFYSPPESFAGSDNESDEEVAGKKSFSAQEREYIRQGKEATAVVDQILAQEENWKFEKNNEYGDTVYTIEVPFHGKTFILKTFLPCPAELVYQEVILQPERMVLWNKTVTACQILQRVEDNTLISYDVSAGAAGGVVSPRDFVNVRRIERRRDRYLSSGIATSHSAKPPTHKYVRGENGPGGFIVLKSASNPRVCTFVWILNTDLKGRLPRYLIHQSLAATMFEFAFHLRQRISELGARA"));
//        Optional<String> id = service.run(request);
//        Assert.assertTrue(id.isPresent());
//
//    }
    
    @Disabled
    @Test
    public void testResult() {
        InterProscanService service = new InterProscanServiceRest();
        Optional<Document> result = service.result("iprscan5-R20150629-154703-0868-18931209-oy");
        assertTrue(result.isPresent());
        Document document = result.get();
    }

    @Disabled
    @Test
    public void testGetApplications() {
        InterProscanService service = new InterProscanServiceRest();
        assertNotNull(service.getApplications());
        assertFalse(service.getApplications().getValues().getValue().isEmpty());
    }
}
