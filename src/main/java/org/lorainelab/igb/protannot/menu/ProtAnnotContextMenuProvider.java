package org.lorainelab.igb.protannot.menu;

import org.lorainelab.igb.menu.api.model.MenuIcon;
import com.affymetrix.genometry.symmetry.impl.GraphSym;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import org.lorainelab.igb.menu.api.AnnotationContextMenuProvider;
import org.lorainelab.igb.menu.api.model.AnnotationContextEvent;
import org.lorainelab.igb.menu.api.model.ContextMenuItem;
import org.lorainelab.igb.menu.api.model.ContextMenuSection;
import org.lorainelab.igb.protannot.ProtAnnotAction;
import org.osgi.service.component.ComponentFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author ShamikaKulkarni
 */
@Component(immediate = true)
public class ProtAnnotContextMenuProvider  implements AnnotationContextMenuProvider {

    private static final Logger LOG = LoggerFactory.getLogger(ProtAnnotMenuProvider.class);
    private static final String PROTANNOT_MENU_ITEM_LABEL = "Start ProtAnnot";
    private static final int MENU_WEIGHT = 20;
    private ComponentFactory protannotFactory;
    
    public static final String PROTANNOT_ICON = "images/protannot.png";

    @Reference(target = "(component.factory=protannot.factory.provider)")
    public void setProtannotFactory(final ComponentFactory protannotFactory) {
        this.protannotFactory = protannotFactory;
    }

    @Override
    public Optional<List<ContextMenuItem>> buildMenuItem(AnnotationContextEvent event) {
        ContextMenuItem protAnnotActionMenuItem = null;
        List<SeqSymmetry> selectedItems = event.getSelectedItems();
        if (!selectedItems.isEmpty() && !(selectedItems.get(0) instanceof GraphSym)) {

            protAnnotActionMenuItem = new ContextMenuItem(PROTANNOT_MENU_ITEM_LABEL, (Void t) -> {
                final Hashtable<String, ?> props = new Hashtable<>();
                ProtAnnotAction instance = (ProtAnnotAction) protannotFactory.newInstance(props).getInstance();
                instance.actionPerformed(null);
                return t;
            });
            try (InputStream resourceAsStream = ProtAnnotMenuProvider.class.getClassLoader().getResourceAsStream(PROTANNOT_ICON) ) {
                protAnnotActionMenuItem.setMenuIcon(new MenuIcon(resourceAsStream));
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
            
            protAnnotActionMenuItem.setWeight(MENU_WEIGHT);
            protAnnotActionMenuItem.setMenuSection(ContextMenuSection.APP);
        }
        return Optional.ofNullable(Arrays.asList(protAnnotActionMenuItem));
    }


}
